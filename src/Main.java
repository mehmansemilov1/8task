import java.util.List;

public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("Tutunamayanlar", "Oguz Atay", "postmodern", 2020);
        Book book2 = new Book("Kürk Mantolu Madonna", "Sabahattin Ali", "postmodern", 2009);
        Book book3 = new Book("Sefiller", "Viktor Hugo", "epik roman", 1925);

        Library library = new Library();

        library.addBook(book1);
        library.addBook(book2);
        library.addBook(book3);

        System.out.println("Kitabxana inventar:");
        library.sortByTitle().forEach(book -> System.out.println(book.getTitle()));

        String searchTitle = "Tutunamayanlar";
        System.out.println("\n kitablar axtarılır: " + searchTitle);
        List<Book> foundBooks = library.searchByTitle(searchTitle);
        foundBooks.forEach(book -> System.out.println(book.getTitle() + " by " + book.getAuthor()));

        System.out.println("\nOrta nəşr ili: " + library.calculateAveragePublicationYear());

        System.out.println("Kitabların sayı: " + library.countBooks());
    }
}
